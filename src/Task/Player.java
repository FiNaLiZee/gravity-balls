package Task;

import com.wizylab.duck2d.Graph;

import java.awt.*;

public class Player {
    private Color color = new Color((int) (Math.random() * 256), (int) (Math.random() * 256), (int) (Math.random() * 256));
    public double velocity = 0;
    public int x, y, size;

    public Player(int x, int y) {
        this.size = (int) (Math.random() * 50) + 10;
        this.x = x;
        this.y = y;
    }

    public void draw(Graph g) {
        g.setColor(color);
        g.fillCircle(x, y, size);
    }
}
