package Task;

import com.wizylab.duck2d.Graph;

import java.awt.*;

public class Rect {
    private static int x;
    private static int y;
    private static int w;
    private static int h;

    public Rect(int x, int y, int w, int h) {
        Rect.x = x;
        Rect.y = y;
        Rect.w = w;
        Rect.h = h;
    }

    public void draw(Graph g) {
        g.setColor(Color.GREEN);
        g.fillRect(x, y, x + w, y + h);
    }
}
