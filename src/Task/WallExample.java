package Task;

import com.wizylab.duck2d.Game;
import com.wizylab.duck2d.Graph;
import com.wizylab.duck2d.Keyboard;
import com.wizylab.duck2d.View;

import java.awt.*;
import java.awt.event.KeyEvent;


public class WallExample implements View {
    public static final int SIZE = 10;
    double x = 100, y = 100;
    int[][] walls = {
            {300, 300, 50, 50},
            {200, 500, 100, 50},
            {500, 100, 50, 100},
    };

    public static void main(String[] args) {
        Game.start(new WallExample());
    }

    @Override
    public void onTimer(long t) {
        if (Keyboard.onKey(KeyEvent.VK_ESCAPE)) System.exit(0);
        if (Keyboard.hasKey(KeyEvent.VK_D) && !isWall(x + 0.5 * t, y)) x += 0.5 * t;
        if (Keyboard.hasKey(KeyEvent.VK_A) && !isWall(x - 0.5 * t, y)) x -= 0.5 * t;
        if (Keyboard.hasKey(KeyEvent.VK_W) && !isWall(x, y - 0.5 * t)) y -= 0.5 * t;
        if (Keyboard.hasKey(KeyEvent.VK_S) && !isWall(x, y + 0.5 * t)) y += 0.5 * t;
    }

    private boolean isWall(double x, double y) {
        if (x < SIZE || y < SIZE || x > 800 - SIZE || y > 600 - SIZE) return true;

        Rectangle hitbox = new Rectangle((int) x - SIZE, (int) y - SIZE, 2 * SIZE, 2 * SIZE);
        for (int[] wall : walls)
            if (hitbox.intersects(new Rectangle(wall[0], wall[1], wall[2], wall[3]))) return true;

        return false;
    }

    @Override
    public void onDraw(Graph g) {
        g.setColor(Color.YELLOW);
        g.fillCircle((int) x, (int) y, SIZE);
        g.setColor(Color.RED);
        for (int[] wall : walls)
            g.fillRect(wall[0], wall[1], wall[0] + wall[2], wall[1] + wall[3]);
    }
}