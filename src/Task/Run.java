package Task;

import com.wizylab.duck2d.Game;
import game2.FreeWorldView;

public class Run {
    public static void main(String[] args) {
        Game.addView(new FreeWorldView());
        Game.start(FreeWorldView.class);
    }
}
