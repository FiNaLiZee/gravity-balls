package Task.one;

import com.wizylab.duck2d.Game;
import com.wizylab.duck2d.Graph;
import com.wizylab.duck2d.Keyboard;
import com.wizylab.duck2d.View;

import java.awt.*;
import java.awt.event.KeyEvent;

public class BulletTestView implements View {
    double x = 100, y = 100, sx = 0, sy = 0, speed = 0.5;
    BulletJump[] bullets = new BulletJump[100];



    public static void main(String[] args) {
        Game.start(new BulletTestView());
    }

    @Override
    public void onShow() {
        for (int i = 0; i < bullets.length; i++) bullets[i] = new BulletJump(100,100,0,1,30);
    }



    @Override
    public void onTimer(long t) {
        if (Keyboard.onKey(KeyEvent.VK_ESCAPE)) System.exit(0);
        if (Keyboard.hasKey(KeyEvent.VK_D)) x += speed * t;
        if (Keyboard.hasKey(KeyEvent.VK_A)) x -= speed * t;
        if (Keyboard.hasKey(KeyEvent.VK_W)) y -= speed * t;
        if (Keyboard.hasKey(KeyEvent.VK_S)) y += speed * t;
        if (Keyboard.onKey(KeyEvent.VK_SPACE))
            for (BulletJump bullet : bullets) {
                if (!bullet.isFree()) continue;
            bullet.x = x;
            bullet.y = y;
                break;
            }

        for (BulletJump bullet : bullets)
            if (!bullet.isFree()) bullet.y += 2 * speed;

        if (x - sx > 700) sx += speed * t;
        if (x - sx < 100) sx -= speed * t;
        if (y - sy < 100) sy -= speed * t;
        if (y - sy > 500) sy += speed * t;
    }

    @Override
    public void onDraw(Graph g) {
        g.putImage("backgrounds/3/background", -sx, -sy, 2750, 1080);
        for (BulletJump bullet : bullets) bullet.draw(g, sx, sy);

        g.setColor(Color.YELLOW);
        g.fillCircle((int) (x - sx), (int) (y - sy), 25);
    }
}
