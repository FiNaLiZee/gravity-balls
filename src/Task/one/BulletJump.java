package Task.one;

import com.wizylab.duck2d.Graph;

public class BulletJump {
    public double x, y, velocity, rebound, size;

    public BulletJump(double x, double y, double velocity, double rebound, double size) {
        this.x = x;
        this.y = y;
        this.velocity = velocity;
        this.rebound = 1;
        this.size = size;
    }

  public boolean isFree() {
     return y >= 1200;
 }

    public void turnOf() {
        x = y = -100;
        velocity = 0;
        rebound = 1;
    }

    public void draw(Graph g, double sx, double sy) {
        g.fillCircle((int) (x - sx), (int) (y - sy), (int) size);
    }
}
