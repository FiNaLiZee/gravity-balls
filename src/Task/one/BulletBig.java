package Task.one;

import com.wizylab.duck2d.Graph;

public class BulletBig {
    public double x, y, velocity, size;

    public BulletBig(double x, double y, double velocity, double size) {
        this.x = x;
        this.y = y;
        this.velocity = velocity;
        this.size = size;
    }

    public boolean isFree() {
        return y >= 1200;
    }

    public void turnOf() {
        x = y = -100;
        velocity = 0;
    }

    public void draw(Graph g, double sx, double sy) {
        g.fillCircle((int) (x - sx), (int) (y - sy), (int) size);
    }
}
