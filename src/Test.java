import com.wizylab.duck2d.Game;
import com.wizylab.duck2d.Graph;
import com.wizylab.duck2d.Keyboard;
import com.wizylab.duck2d.View;

import java.awt.*;
import java.awt.event.KeyEvent;

public class Test implements View {
    double x = 100, y = 100, speed = 0.5;
    double sx = 0, sy = 0;
    int[][] walls = {
            {200, 200, 50, 50},
            {400, 400, 50, 50},
            {600, 600, 50, 50}
    };

    public static void main(String[] args) {
        Game.start(new Test());
    }

    @Override
    public void onTimer(long t) {
        if (Keyboard.onKey(KeyEvent.VK_ESCAPE)) System.exit(0);

        if (Keyboard.hasKey(KeyEvent.VK_LEFT)) x -= speed * t;
        if (x - sx < 100) sx -= speed * t;

        if (Keyboard.hasKey(KeyEvent.VK_RIGHT)) x += speed * t;
        if (Keyboard.hasKey(KeyEvent.VK_UP)) y -= speed * t;
        if (Keyboard.hasKey(KeyEvent.VK_DOWN)) y += speed * t;
        if (x - sx > 700 - 20) sx += speed * t;
        if (y - sy < 100) sy -= speed * t;
        if (y - sy > 500 - 20) sy += speed * t;
    }

    @Override
    public void onDraw(Graph g) {
        g.setFillColor(Color.YELLOW);
        g.fillRect((int) (x - sx), (int) (y - sy), (int) (x - sx + 20), (int) (y - sy + 20));

        g.setFillColor(Color.RED);
        for (int[] wall : walls)
            g.fillRect(wall[0] - (int) sx, wall[1] - (int) sy, wall[0] + wall[2] - (int) sx, wall[1] + wall[3] - (int) sy);
    }
}