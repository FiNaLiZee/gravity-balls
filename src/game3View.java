import com.wizylab.duck2d.Environment;
import com.wizylab.duck2d.Graph;
import com.wizylab.duck2d.Keyboard;
import com.wizylab.duck2d.View;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class game3View implements View {
    public static void main(String[] args) {

    }
    private static final int WIDTH = 1920, HEIGHT = 1080;
    private static final int widthP = 100, heightP = 200;
    int x1 = 100;
    int y1 = 100;
    int ss;
    int[][] walls;

     @Override
    public void onTimer(long t) {
         load(Environment.get("level", 2));
         if (Keyboard.onKey(KeyEvent.VK_ESCAPE))  System.exit(0);
         if ((Keyboard.hasKey(KeyEvent.VK_D)) && !isWall(x1 + 0.5 * t, y1,0,0)) {
             x1 += 0.5 * t;}
         if ((Keyboard.hasKey(KeyEvent.VK_A)) && !isWall(x1 - 0.5 * t, y1, 0, 0)) x1 -= 0.5 * t;
         if ((Keyboard.hasKey(KeyEvent.VK_W)) && !isWall(x1, y1 - 0.5 * t, 0, 0)) y1 -= 0.5 * t;
         if ((Keyboard.hasKey(KeyEvent.VK_S)) && !isWall(x1, y1 + 0.5 * t, 0, 0)) y1 += 0.5 * t;
         ss += t;
         if (ss > 2800) ss = 50;
    }

    private boolean isWall(double x, double y, double width, double height) {
        return !(x > 0 && y > 0 && x < WIDTH - widthP && y < HEIGHT - heightP);
        Rectangle hitbox = new Rectangle((int) x-6, (int)y-4, (int) widthP*2, (int) heightP*2);
        for (int[] wall : walls) {
            if (hitbox.intersects(new Rectangle(wall[0], wall[1], wall[2], wall[3]))) return true;
        }
    }

    @Override
    public void onDraw(Graph g) {
      //   g.fillCircle(x1,y1,50);
        g.putImage("animation/pers", x1, y1,widthP,heightP);
        g.setColor(Color.cyan);
        for (int[] wall : walls) {
            g.fillRect(wall[0], wall[1], wall[0] + wall[2], wall[1] + wall[3]);
        }
    }

    public void load(int level) {
        try (Scanner sc = new Scanner(new File("maps/map" + level + ".txt"))) {
            walls = new int[sc.nextInt()][4];
            for (int i = 0; i < walls.length; i++)
                walls[i] = new int[]{sc.nextInt(), sc.nextInt(), sc.nextInt(), sc.nextInt()};
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
