package other;

import com.wizylab.duck2d.Graph;

import java.awt.*;

public class Bonus {
    public int x, y, w, h;
    public double speed;

    public Bonus (int x, int y, int w, int h) {
        this.x = x;
        this.y = y;
        this.w = w;
        this.h = h;
    }

    public Rectangle getHB() {
        return new Rectangle(x, y, w, h);
    }

    public void draw(Graph g) {
        g.setColor(Color.magenta);
        g.fillRect((int) Math.random()+5, y, x + w, y + h);
    }
}
