package other;

import com.wizylab.duck2d.Graph;

import java.awt.*;

public class Shar {

    public static double velocity = 0;
    public static int size;
    public static int y;
    public static int x;

    public Shar(int x, int y, int size) {
        this.size = size;
        this.x = x;
        this.y = y;
    }

    public void draw(Graph g) {
        g.setColor(Color.RED);
        g.fillCircle(x, y, size);
    }
}
