package other;

import Task.Wall;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Test {
    List<Wall> walls;

    public static void main(String[] args) {
        new Test().onShow();
    }

    public void onShow() {
        load("map1");
        System.out.println(walls);
    }

    public void load(String name) {
        walls = new ArrayList<>();
        try {
            Scanner sc = new Scanner(new File("maps/" + name + ".txt"));
            while (sc.hasNextLine()) {
                walls.add(new Wall(sc.nextInt(), sc.nextInt(), sc.nextInt(), sc.nextInt()));
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
