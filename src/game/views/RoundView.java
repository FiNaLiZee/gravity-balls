package game.views;

import com.wizylab.duck2d.*;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class RoundView implements View{
    private final double GRAVITY = 0.075, JUMP_POWER = 6;
    private final int SIZE = 10;
    private double velocity1, velocity2;
    private double p1x, p1y, p2x, p2y, Cx, Cy;
    private double Ox, Oy;
    private double Os;
    private double timer;
    private double counter;
    private int playerColor;
    int round, fon;
    private int[][] walls;
    private Color color1;
    private Color color2;
    int counterRound1;
    int counterRound2;

    @Override
    public void onShow() {
        load(Environment.get("level", 4));
        velocity1 = 0;
        velocity2 = 0;
        round = 1;
        p1x = 30;
        p1y = 50;
        p2x = 1800;
        p2y = 50;
        Cx = 960;
        Cy = 100;
        Os = SIZE+15;
        timer = 0;
        counter = 2999;
        playerColor = 1;
        counterRound1=0;
        counterRound2=0;
        fon = 1;
    }

    @Override
    public void onTimer (long t) {
        if (Keyboard.onKey(KeyEvent.VK_ESCAPE)) Game.show(MenuView.class);
        if ((Keyboard.onKey(KeyEvent.VK_O)) ) fon *= -1 ;
        if (counter < 0) {
            if ((Keyboard.hasKey(KeyEvent.VK_D)) && !isWall(p1x + 0.5 * t, p1y)) p1x += 0.5 * t;
            if ((Keyboard.hasKey(KeyEvent.VK_A)) && !isWall(p1x - 0.5 * t, p1y)) p1x -= 0.5 * t;
            if ((Keyboard.hasKey(KeyEvent.VK_RIGHT)) && !isWall(p2x + 0.5 * t, p2y)) p2x += 0.5 * t;
            if ((Keyboard.hasKey(KeyEvent.VK_LEFT)) && !isWall(p2x - 0.5 * t, p2y)) p2x -= 0.5 * t;

            if (Keyboard.onKey(KeyEvent.VK_SPACE) && velocity1 == 0) velocity1 -= JUMP_POWER;
            if (Keyboard.onKey(KeyEvent.VK_UP) && velocity2 == 0) velocity2 -= JUMP_POWER;
        }

        color1 = Color.RED;
        color2 = Color.green;

        velocity1 += GRAVITY;
        velocity2 += GRAVITY;

        if (!isWall(p1x, p1y + velocity1)) p1y += velocity1;
        else velocity1 = 0;
        if (!isWall(p2x, p2y + velocity2)) p2y += velocity2;
        else velocity2 = 0;

        counter -= t;
        if (counter < 0) {
            timer += t;
            Cx = -100;
        }
        if (SIZE + Os >= Math.sqrt((p1x - p2x) * (p1x - p2x) + (p1y - p2y) * (p1y - p2y))) {
            if (playerColor == 1)counterRound1++;
            if (playerColor == -1)counterRound2++;
            timer = 0; counter  = 3000; p1x = 30; p1y = 60; p2x = 1800; p2y = 60; Cx = 960; round++; playerColor *= -1;
        }
            if (p1y > 1020) {counterRound1--; timer = 0; counter  = 3000; p1x = 30; p1y = 60; p2x = 1800; p2y = 60; Cx = 960; round++;}
            if (p2y > 1020) {counterRound2--; timer = 0; counter  = 3000; p1x = 30; p1y = 60; p2x = 1800; p2y = 60; Cx = 960; round++;}

        if (timer > 10000 && playerColor == -1) {counterRound1++; timer = 0; counter  = 3000; p1x = 30; p1y = 60; p2x = 1800; p2y = 60; Cx = 960; round++; playerColor *= -1;}
        if (timer > 10000 && playerColor == 1) {counterRound2++; timer = 0; counter  = 3000; p1x = 30; p1y = 60; p2x = 1800; p2y = 60; Cx = 960; round++; playerColor *= -1;}

        if (playerColor == 1) {
            color1 = Color.RED;
            color2 = Color.green;
            Ox = p1x;
            Oy = p1y;
        }
        if (playerColor == -1) {
            color1 = Color.green;
            color2 = Color.RED;
            Ox = p2x;
            Oy = p2y;
        }

    }

    private boolean isWall(double x, double y) {
        if (x < SIZE || y < SIZE || 1920 - SIZE < x || 1060 - SIZE < y) return true;

        Rectangle hitbox = new Rectangle((int) x - SIZE, (int) y - SIZE, 2 * SIZE, 2 * SIZE);
        for (int[] wall : walls) {
            if (hitbox.intersects(new Rectangle(wall[0], wall[1], wall[2], wall[3]))) return true;
        }
        return false;
    }

    @Override
    public void onDraw(Graph g) {
        if (fon == 1) {g.putImage("backgrounds/2/background1", 0, 0, 1920, 1080);};
        if (fon == -1) {g.putImage("backgrounds/2/background2", 0, 0, 1920, 1080);};
        g.setColor(Color.white);
        g.setTextStyle(1, 1, 30);
        g.ctext(960, 10, 960, (int) 10," Раунд:"+ round);
        g.setColor(Color.red);
        int sec = (int) ((counter / 1000)+1);
        g.setTextStyle(1, 1, 75);
        g.ctext((int) Cx, (int) Cy, (int) Cx, (int) Cy, String.valueOf(sec));
        g.setColor(Color.white);
        g.ctext( 960,  400,  960,  400, counterRound1 + ":" + counterRound2);

        g.setTextStyle(1, 1, 17);
        g.setColor(Color.white);
        g.setTextStyle(1, 1, 25);
        int secT = (int) (timer / 1000), msT = (int) (timer % 1000 / 100);
        g.ctext(960, 40, 960, 40, secT + "." + msT);
        g.setColor(color1);
        g.fillCircle((int) p1x, (int) p1y, SIZE);
        g.setColor(color2);
        g.fillCircle((int) p2x, (int) p2y, SIZE);
        g.setColor(Color.cyan);
        for (int[] wall : walls) {
            g.fillRect(wall[0], wall[1], wall[0] + wall[2], wall[1] + wall[3]);
        }
        g.setColor(Color.red);
        g.circle((int) Ox, (int) Oy, (int) Os);
        g.setColor(Color.white);
        g.ctext((int) p1x, (int) p1y - 50, (int) p1x, (int) p1y, "ONE");
        g.ctext((int) p2x, (int) p2y - 50, (int) p2x, (int) p2y, "TWO");
    }

    public void load(int level) {
        try (Scanner sc = new Scanner(new File("maps/map" + level + ".txt"))) {
            walls = new int[sc.nextInt()][4];
            for (int i = 0; i < walls.length; i++)
                walls[i] = new int[]{sc.nextInt(), sc.nextInt(), sc.nextInt(), sc.nextInt()};
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
