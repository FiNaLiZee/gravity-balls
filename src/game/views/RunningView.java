package game.views;

import com.wizylab.duck2d.Game;
import com.wizylab.duck2d.Graph;
import com.wizylab.duck2d.Keyboard;
import com.wizylab.duck2d.View;

import java.awt.event.KeyEvent;

public class RunningView implements View {
    private static final int WIDTH = 1920, HEIGHT = 1080;
    private static final int MAPWIDTH = 2750, MAPHEIGHT = 1080;
    private static final int GUNWIDTHT = 410, GUNHEIGHT = 150;
    double xPushka, yPushka;
    double speed;
    double sx, sy;

    @Override
    public void onShow() {
        sx = 150;
        sy = 0;
        xPushka = 600;
        yPushka = 150;
        speed = 1.5;
    }


    @Override
    public void onTimer(long t) {
        if (Keyboard.onKey(KeyEvent.VK_ESCAPE)) Game.show(MenuView.class);
        if ((Keyboard.hasKey(KeyEvent.VK_D)) && !isWall(xPushka + speed * t, yPushka, GUNWIDTHT, 0)) xPushka += speed * t;
        if ((Keyboard.hasKey(KeyEvent.VK_A)) && !isWall(xPushka - speed * t, yPushka, GUNWIDTHT, 0)) xPushka -= speed * t;
        if (sx + WIDTH + speed * t <= MAPWIDTH && xPushka - sx >= WIDTH - GUNWIDTHT - 30) sx += speed * t;
        if (sx - speed * t >= 0  && xPushka - sx - 30 <= 0) sx -= speed * t;
    }

    private boolean isWall(double x, double y, double width, double height) {
        return !(x > 0 && y > 0 && x < MAPWIDTH - width && y < MAPHEIGHT - height);
    }


    @Override
    public void onDraw(Graph g) {
        g.putImage("backgrounds/3/background", -sx, 0, MAPWIDTH, MAPHEIGHT);
        g.putImage("skins/blast_gun", (int) (xPushka - sx), (int) yPushka, GUNWIDTHT, GUNHEIGHT);
    }

}

