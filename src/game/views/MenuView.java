package game.views;

import com.wizylab.duck2d.Game;
import com.wizylab.duck2d.Graph;
import com.wizylab.duck2d.Keyboard;
import com.wizylab.duck2d.View;
import game2.FreeWorldView;

import java.awt.*;
import java.awt.event.KeyEvent;

public class MenuView implements View {
   private static final String[] TITLES = {"ДОГОНЯЛКИ","РАУНДЫ","ЦЕЛЫЙ МИР","ABOUT","ВЫХОД"};
    private static final Rectangle[] BUTTONS = {
            new Rectangle(840, 400, 250, 100),
            new Rectangle(840, 525, 250, 100),
            new Rectangle(840, 650, 250, 100),
            new Rectangle(840, 775, 250, 100),
            new Rectangle(840, 900, 250, 100)
    };
    int value = 0;
    @Override
    public void onTimer(long l) {
        if (Keyboard.onKey(KeyEvent.VK_ESCAPE)) System.exit(0);
        if (Keyboard.onKey(KeyEvent.VK_UP)) value = (value == 0) ? BUTTONS.length - 1 : value - 1;
        if (Keyboard.onKey(KeyEvent.VK_DOWN)) value = (value == BUTTONS.length - 1) ? 0 : value + 1;
        if (Keyboard.onKey(KeyEvent.VK_ENTER)) {
            if (value == 0) Game.show(GameView.class);
            if (value == 1) Game.show(RoundView.class);
            if (value == 2) Game.show(FreeWorldView.class);
            if (value == 3) Game.show(AboutView.class);
            if (value == 4) System.exit(0);
        }
    }

    @Override
    public void onDraw(Graph g) {
        g.putImage("backgrounds/menu", 0, 0, 1920, 1080);
        g.setColor(Color.black);
        g.setTextStyle(2, 1, 20);
        for (int i = 0; i < BUTTONS.length; i++) {
            Rectangle b = BUTTONS[i];
            String tx = "btns/btn0" + ((i == value) ? 1 : 0);
            g.putImage(tx, b.x, b.y, b.width, b.height);
            g.ctext(b, TITLES[i]);
        }
        g.setColor(Color.YELLOW);
        g.setTextStyle(1, 1, 200);
    }
}
