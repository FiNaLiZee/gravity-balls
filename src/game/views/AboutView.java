package game.views;

import com.wizylab.duck2d.*;

import java.awt.*;
import java.awt.event.KeyEvent;

public class AboutView implements View{

     @Override
    public void onTimer(long t) {
        if (Keyboard.onKey(KeyEvent.VK_ESCAPE)) Game.show(MenuView.class);
    }

    @Override
    public void onDraw(Graph g) {
        g.setColor(Color.white);
        g.setTextStyle(1, 1, 20);
        g.putImage("backgrounds/about", 0, 0, 1920, 1020);

    }
}
