package game.views;

import com.wizylab.duck2d.*;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;


public class GameView implements View {
    public static final int HEIGHT = 1080, WIDTH = 1920;
    public static final double GRAVITY = 0.075, JUMP_POWER = 6;
    public static final int SIZE = 10;
    double velocity1, velocity2, gravityDirection;
    long TimeColor;
    double p1x, p1y, p2x, p2y;
    double p1sx, p2sx;
    long gravityChangeTime;
    double Ox, Oy, Os;
    double TimeSR, TimerSpeedRed, TimerStanRed;
    double timer, counterSpeed, counterStan;
    int playerColor, speedButton, stanButton;

    int[][] walls;
    private Color color1;
    private Color color2;

    @Override
    public void onShow() {
        load(Environment.get("level", 1));

        p1x = 200;
        p1y = 150;
        p1sx = 0.5;

        p2x = 1300;
        p2y = 200;
        p2sx = 0.5;

        velocity1 = 0;
        velocity2 = 0;
        gravityDirection = 1;
        gravityChangeTime = 0;

        speedButton = 0;
        stanButton = 0;

        TimeSR = 1;
        TimerSpeedRed = 7000;
        TimerStanRed = 10000;

        Os = SIZE + 9;
        timer = 0;
        TimeColor = 0;
        playerColor = 1;
        counterSpeed = 7000;
        counterStan = 7000;
    }

    @Override
    public void onTimer(long t) {
        if (Keyboard.onKey(KeyEvent.VK_ESCAPE)) Game.show(MenuView.class);
        if ((Keyboard.hasKey(KeyEvent.VK_D)) && !isWall(p1x + p1sx * t, p1y)) p1x += p1sx * t;
        if ((Keyboard.hasKey(KeyEvent.VK_A)) && !isWall(p1x - p1sx * t, p1y)) p1x -= p1sx * t;
        if ((Keyboard.hasKey(KeyEvent.VK_RIGHT)) && !isWall(p2x + p2sx * t, p2y)) p2x += p2sx * t;
        if ((Keyboard.hasKey(KeyEvent.VK_LEFT)) && !isWall(p2x - p2sx * t, p2y)) p2x -= p2sx * t;

        counterSpeed -= t;
        counterStan -= t;

        TimerStanRed += t;
        if ((Keyboard.onKey(KeyEvent.VK_Q) && (playerColor == -1) && TimerSpeedRed > 3000) && TimerStanRed > 10000) {
            counterStan = 10000;
            TimerStanRed = 0;

        }

        if ((Keyboard.onKey(KeyEvent.VK_SHIFT) && (playerColor == 1) && TimerSpeedRed > 3000) && TimerStanRed > 10000) {
            counterStan = 10000;
            TimerStanRed = 0;
        }

        TimerSpeedRed += t;
        if ((Keyboard.onKey(KeyEvent.VK_E) && (playerColor == 1)) && TimerSpeedRed > 7000 && TimerStanRed > 3000) {
            counterSpeed = 7000;
            TimerSpeedRed = 0;
        }

        if ((Keyboard.onKey(KeyEvent.VK_ENTER) && (playerColor == -1)) && TimerSpeedRed > 7000 && TimerStanRed > 3000) {
            counterSpeed = 7000;
            TimerSpeedRed = 0;
        }
        if (TimerSpeedRed > 7000) {
            speedButton = 1;
        } else {
            speedButton = 0;
        }
        if (TimerStanRed > 10000) {
            stanButton = 1;
        } else {
            stanButton = 0;
        }

        if (TimerSpeedRed > 3000 && TimerStanRed < 3000 && playerColor == -1) {
            p2sx = 0;
            p1sx = 0.5;
        }
        if (TimerSpeedRed > 3000 && TimerStanRed < 3000 && playerColor == 1) {
            p2sx = 0.5;
            p1sx = 0;
        }

        if (TimerSpeedRed > 3000 && TimerStanRed > 3000 && playerColor == -1) {
            p2sx = 0.5;
            p1sx = 0.5;
        }
        if (TimerSpeedRed > 3000 && TimerStanRed > 3000 && playerColor == 1) {
            p2sx = 0.5;
            p1sx = 0.5;
        }

        if (TimerSpeedRed < 3000 && TimerStanRed > 3000 && playerColor == -1) {
            p2sx = 1;
            p1sx = 0.5;
        }
        if (TimerSpeedRed < 3000 && TimerStanRed > 3000 && playerColor == 1) {
            p2sx = 0.5;
            p1sx = 1;
        }


        gravityChangeTime += t;
        if (gravityChangeTime > 10000) {
            gravityDirection *= -1;
            gravityChangeTime = 0;
        }

        if (playerColor == 1) {
            if (Keyboard.onKey(KeyEvent.VK_UP) && velocity2 == 0) velocity2 -= gravityDirection * JUMP_POWER;
            if (counterStan < 7000) {
                if (Keyboard.onKey(KeyEvent.VK_SPACE) && velocity1 == 0) velocity1 -= gravityDirection * JUMP_POWER;
            }
        }
        if (playerColor == -1) {
            if (counterStan < 7000) {
                if (Keyboard.onKey(KeyEvent.VK_UP) && velocity2 == 0) velocity2 -= gravityDirection * JUMP_POWER;
            }
            if (Keyboard.onKey(KeyEvent.VK_SPACE) && velocity1 == 0) velocity1 -= gravityDirection * JUMP_POWER;
        }

        color1 = Color.RED;
        color2 = Color.green;

        velocity1 += gravityDirection * GRAVITY;
        velocity2 += gravityDirection * GRAVITY;

        if (!isWall(p1x, p1y + velocity1)) p1y += velocity1;
        else velocity1 = 0;
        if (!isWall(p2x, p2y + velocity2)) p2y += velocity2;
        else velocity2 = 0;

        timer += t;
        if (timer > 10000) timer = 0;
        TimeColor += t;

        if (SIZE + Os >= Math.sqrt((p1x - p2x) * (p1x - p2x) + (p1y - p2y) * (p1y - p2y)) && TimeColor > 1000) {
            playerColor *= -1;
            TimeColor = 0;
        }
        if (playerColor == 1) {
            color1 = Color.RED;
            color2 = Color.green;
            Ox = p1x;
            Oy = p1y;
        }
        if (playerColor == -1) {
            color1 = Color.green;
            color2 = Color.RED;
            Ox = p2x;
            Oy = p2y;
        }
    }

    private boolean isWall(double x, double y) {
        if (x < SIZE || y < SIZE || 1920 - SIZE < x || 1060 - SIZE < y) return true;

        Rectangle hitbox = new Rectangle((int) x - SIZE, (int) y - SIZE, 2 * SIZE, 2 * SIZE);
        for (int[] wall : walls) {
            if (hitbox.intersects(new Rectangle(wall[0], wall[1], wall[2], wall[3]))) return true;
        }
        return false;
    }

    @Override
    public void onDraw(Graph g) {
        g.putImage("backgrounds/1/background", 0, 0, 1920, 1080);
        g.putImage("abillities/Speed/speed" + ((speedButton == 1) ? '+' : '-'),  1320,  0, 95,  95);
        g.putImage("abillities/Stun/stun" + ((stanButton == 1) ? '+' : '-'),  1420,  0, (int) 95,  95);
        g.setColor(Color.white);
        int sec = (int) (timer / 1000), ms = (int) (timer % 1000 / 100);
        g.ctext(770, 0, 800, 20, sec + "." + ms);
        g.setColor(color1);
        g.fillCircle((int) p1x, (int) p1y, SIZE);
        g.putImage("skins/balls/skin1", (int) p1x-SIZE, (int) p1y-SIZE, 2*SIZE, 2*SIZE);
        g.setColor(color2);
        g.fillCircle((int) p2x, (int) p2y, SIZE);
        g.putImage("skins/balls/skin2", (int) p2x-SIZE, (int) p2y-SIZE, 2*SIZE, 2*SIZE);
        g.setColor(Color.cyan);
        for (int[] wall : walls) {
            g.fillRect(wall[0], wall[1], wall[0] + wall[2], wall[1] + wall[3]);
        }
        g.setColor(Color.red);
        g.circle((int) Ox, (int) Oy, (int) Os);
        g.setColor(Color.white);
        g.ctext((int) p1x, (int) p1y - 50, (int) p1x, (int) p1y, "ONE");
        g.ctext((int) p2x, (int) p2y - 50, (int) p2x, (int) p2y, "TWO");
        g.setColor(Color.white);
        int secSPEED = (int) ((counterSpeed / 1000)+1);
        g.setTextStyle(1, 1, 75);
        if (speedButton == 0) g.ctext((int) 1330, 50,  1410, (int) 50, String.valueOf(secSPEED));
        int secSTAN = (int) ((counterStan / 1000)+1);
        if (stanButton == 0) g.ctext( 1410,  50, 1520, 50, String.valueOf(secSTAN));
    }

    public void load(int level) {
        try (Scanner sc = new Scanner(new File("maps/map" + level + ".txt"))) {
            walls = new int[sc.nextInt()][4];
            for (int i = 0; i < walls.length; i++)
                walls[i] = new int[]{sc.nextInt(), sc.nextInt(), sc.nextInt(), sc.nextInt()};
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
