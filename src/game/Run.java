package game;

import com.wizylab.duck2d.Environment;
import com.wizylab.duck2d.Game;
import game.views.*;
import game2.FreeWorldView;
import game2.PostFreeWorldRView;
import game2.PostFreeWorldZView;

public class Run {
    public static void main(String[] args) {
        Game.addView(
                new MenuView(),
                new AboutView(),
                new GameView(),
                new RoundView(),
                new RunningView(),
                new FreeWorldView(),
                new PostFreeWorldZView(),
                new PostFreeWorldRView()
        );
        Environment.put("window.width", GameView.WIDTH);
        Environment.put("window.height", GameView.HEIGHT);
        Game.start(MenuView.class);
    }
}
