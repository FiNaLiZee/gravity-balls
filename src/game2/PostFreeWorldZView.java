package game2;

import com.wizylab.duck2d.Game;
import com.wizylab.duck2d.Graph;
import com.wizylab.duck2d.Keyboard;
import com.wizylab.duck2d.View;
import game.views.MenuView;

import java.awt.*;
import java.awt.event.KeyEvent;

public class PostFreeWorldZView implements View {
    private static final String[] TITLES = {"ЗАНОВО","МЕНЮ"};
    private static final Rectangle[] BUTTONS = {
            new Rectangle(840, 525, 250, 100),
            new Rectangle(840, 650, 250, 100)
    };

    int value = 0;
    @Override
    public void onTimer(long l) {
        if (Keyboard.onKey(KeyEvent.VK_ESCAPE)) Game.show(MenuView.class);
        if (Keyboard.onKey(KeyEvent.VK_UP)) value = (value == 0) ? BUTTONS.length - 1 : value - 1;
        if (Keyboard.onKey(KeyEvent.VK_DOWN)) value = (value == BUTTONS.length - 1) ? 0 : value + 1;
        if (Keyboard.onKey(KeyEvent.VK_ENTER)) {
            if (value == 0) Game.show(FreeWorldView.class);
            if (value == 1) Game.show(MenuView.class);
        }
    }
    @Override
    public void onDraw (Graph g) {
        g.putImage("backgrounds/3/postBGG", 0,0,1920,1080);
        g.setColor(Color.black);
        g.setTextStyle(2, 1, 20);
        for (int i = 0; i < BUTTONS.length; i++) {
            Rectangle b = BUTTONS[i];
            String tx = "btns/btn0" + ((i == value) ? 1 : 0);
            g.putImage(tx, b.x, b.y, b.width, b.height);
            g.ctext(b, TITLES[i]);
        }
    }
}
