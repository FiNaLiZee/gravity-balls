package game2;

import Task.one.BulletBig;
import Task.one.BulletJump;
import com.wizylab.duck2d.Game;
import com.wizylab.duck2d.Graph;
import com.wizylab.duck2d.Keyboard;
import com.wizylab.duck2d.View;
import game.views.MenuView;

import java.awt.*;
import java.awt.event.KeyEvent;

public class FreeWorldView implements View {
    private static final int WIDTH = 1920, HEIGHT = 1080;
    private static final int MAPWIDTH = 3048, MAPHEIGHT = 1080;
    private static final int GUNWIDTHT = 410, GUNHEIGHT = 150;
    public static final double GRAVITY = 0.075, JUMP_POWER = 6, size = 20;
    private final BulletJump[] bulletsJ = new BulletJump[3];
    private final BulletBig[] bulletsB = new BulletBig[1];
    double sx, sy;
    double xPushka, yPushka, xPl, yPl;
    double speed;
    double velocity;
    double doubleJump;
    int healPh ,healPl;
    double xx,yy;
    double timerRocket;


    @Override
    public void onShow() {
        for (int i = 0; i < bulletsJ.length; i++) bulletsJ[i] = new BulletJump(-200, 800,0,1,30);
        for (int i = 0; i < bulletsB.length; i++) bulletsB[i] = new BulletBig(-200, 800,0,50);
        sx = 150;
        sy = 0;
        xPushka = 600;
        yPushka = 150;
        xPl = 200;
        yPl = 900;
        speed = 1;
        velocity = 0;
        healPl = 3;
        healPh = 3;
        timerRocket=0;
        doubleJump = 2;
        xx = Math.random()*1500+750;
        yy = Math.random()*300+500;
    }

    @Override
    public void onTimer(long t) {
        if (Keyboard.onKey(KeyEvent.VK_ESCAPE)) Game.show(MenuView.class);
        if ((Keyboard.hasKey(KeyEvent.VK_D)) && !isWall(xPushka + speed * t, yPushka, GUNWIDTHT, 0)) xPushka += speed * t;
        if ((Keyboard.hasKey(KeyEvent.VK_A)) && !isWall(xPushka - speed * t, yPushka, GUNWIDTHT, 0)) xPushka -= speed * t;
        if (sx + WIDTH + speed * t <= MAPWIDTH && xPushka - sx >= WIDTH - GUNWIDTHT - 30) sx += speed * t;
        if (sx + size + speed * t <= MAPWIDTH && xPl - sx >= WIDTH - size) sx += speed * t;
        if (sx - speed * t >= 0  && (xPushka - sx - 30 <= 0 || xPl - sx - 30 <=0)) sx -= speed * t;

        if ((Keyboard.hasKey(KeyEvent.VK_RIGHT)) && !isWall(xPl + speed * t,yPl, 0,0)) xPl += speed * t;
        if ((Keyboard.hasKey(KeyEvent.VK_LEFT)) && !isWall(xPl - speed * t, yPl,0,0)) xPl -= speed * t;
        if ((Keyboard.onKey(KeyEvent.VK_UP)) && doubleJump > 0) {velocity =  -JUMP_POWER; doubleJump -=1;}
        if (velocity == 0) doubleJump = 2;
        velocity += GRAVITY;
        if (!isWall(xPl, yPl + velocity,0,0)) yPl += velocity;
        else velocity = 0;

        for (BulletJump bullet : bulletsJ)
            if (bullet.size+size >= Math.sqrt((xPl - bullet.x) * (xPl - bullet.x) + (yPl - bullet.y) * (yPl - bullet.y))) {bullet.turnOf(); healPl -=1;}

        for (BulletBig bullet : bulletsB)
            if (bullet.size+size >= Math.sqrt((xPl - bullet.x) * (xPl - bullet.x) + (yPl - bullet.y) * (yPl - bullet.y))) {bullet.turnOf(); healPl -=2;}

        if (healPl < 1) Game.show(PostFreeWorldRView.class);
        if (healPh < 1) Game.show(PostFreeWorldZView.class);

        if (50+size >= Math.sqrt((xPl - xx) * (xPl - xx) + (yPl - yy) * (yPl - yy))) {healPh -=1;xx = yy = -100;timerRocket=0;}
        if (timerRocket >= 6000) {
            xx = Math.random()*1500+750;
            yy = Math.random()*290+550;
            timerRocket = 0;}
        timerRocket +=t;



        if (Keyboard.onKey(KeyEvent.VK_Q)) {
            for (BulletJump bullet : bulletsJ) {
                if (bullet.x >= 0 && bullet.x < 3048 && bullet.y >= -100 && bullet.y <= 1060) continue;
                bullet.x = (int) (xPushka + 205);
                bullet.y = (int) (yPushka + 75);
                bullet.velocity = -3;
                break;
            }
        }
        if (Keyboard.onKey(KeyEvent.VK_E)) {
            for (BulletBig bullet : bulletsB) {
                if (bullet.x >= 0 && bullet.x < 3048 && bullet.y >= -100 && bullet.y <= 1060) continue;
                bullet.x = (int) (xPushka + 205);
                bullet.y = (int) (yPushka + 75);
                bullet.velocity = -3;
                break;
            }
        }

        for (BulletJump bullet : bulletsJ){
            if (bullet.y > MAPHEIGHT - 55 && bullet.rebound == 1){
                bullet.velocity = -7;
                bullet.rebound = 0;
            }
        }

        for (BulletJump bullet : bulletsJ) {
            if (bullet.x > 0 && bullet.y > 0) bullet.y += bullet.velocity;
            bullet.velocity += GRAVITY;
            if (bullet.y > 1080) bullet.turnOf();
        }

        for (BulletBig bullet : bulletsB) {
            if (bullet.x > 0 && bullet.y > 0) bullet.y += bullet.velocity;
            bullet.velocity += GRAVITY;
            if (bullet.y > 1080) bullet.turnOf();
        }

    }

    private boolean isWall(double x, double y, double width, double height) {
        return !(x > 0 && y > 0 && x < MAPWIDTH - width - size && y < MAPHEIGHT - height - size*1.7);
    }

    @Override
    public void onDraw(Graph g) {
        g.putImage("backgrounds/3/background", -sx, 0, MAPWIDTH, MAPHEIGHT);
        if (healPh == 3)
            g.putImage("skins/blast_gun/3",  (int) (xPushka - sx),  (int) yPushka, (int) GUNWIDTHT,  GUNHEIGHT);
        if (healPh == 2)
            g.putImage("skins/blast_gun/2",  (int) (xPushka - sx),  (int) yPushka, (int) GUNWIDTHT,  GUNHEIGHT);
        if (healPh == 1)
            g.putImage("skins/blast_gun/1",  (int) (xPushka - sx),  (int) yPushka, (int) GUNWIDTHT,  GUNHEIGHT);

        g.setColor(Color.orange);
        for (BulletJump bullet : bulletsJ) bullet.draw(g, sx, sy);
        g.setColor(Color.red);
        for (BulletBig bullet : bulletsB) bullet.draw(g, sx, sy);
        g.setColor(Color.green);
        g.fillCircle((int)(xPl - sx),(int)yPl,(int) size);
        g.setColor(Color.black);
        g.ctext((int)(xPl-sx), (int)(yPl-sy), (int)(xPl-sx), (int)(yPl-sy), String.valueOf(healPl));
        g.setColor(Color.CYAN);
        g.fillCircle((int) (xx-sx),(int) (yy-sy),45);
        g.setColor(Color.black);
        g.text((int)((xx-35)-sx), (int)(yy-sy),"ATTACK");
    }
}
