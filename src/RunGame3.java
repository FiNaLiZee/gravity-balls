import com.wizylab.duck2d.Environment;
import com.wizylab.duck2d.Game;
import game.views.GameView;

public class RunGame3 {
        public static void main(String[] args) {
            Game.addView(
                    new game3View()
            );
            Environment.put("window.width", GameView.WIDTH);
            Environment.put("window.height", GameView.HEIGHT);
            Game.start(game3View.class);
        }
    }
